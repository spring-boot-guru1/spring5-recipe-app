package br.com.mrb.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.mrb.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}
