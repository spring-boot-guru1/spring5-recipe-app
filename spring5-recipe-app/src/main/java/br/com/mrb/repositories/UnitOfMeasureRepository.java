package br.com.mrb.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.mrb.domain.UnitOfMeasure;
import java.util.Optional;;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {

	Optional<UnitOfMeasure> findByDescription(String description);
	
}