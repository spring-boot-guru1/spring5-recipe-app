package br.com.mrb.services;

import java.util.Set;

import br.com.mrb.domain.Recipe;

public interface RecipeService {

	Set<Recipe> getRecipes();
}